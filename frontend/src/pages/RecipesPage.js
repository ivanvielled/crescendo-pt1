import React, { useState, useEffect } from 'react'
import {
    Container,
    Button,
    ButtonGroup,
    InputGroup,
    Card,
    Row,
    Col
} from 'react-bootstrap'

export default function RecipePage() {
    const [recipes, setRecipes] = useState([])
    const NodeServerURL = 'http://localhost:3001/'

    useEffect(() => {
        fetch(`http://localhost:3001/recipes`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if (data !== null) {
                setRecipes(data)
            }
        })
    }, [])

    // console.log(recipes)

    const recipeData = recipes.map(data => {
        console.log(data)
        return (
            <Card key={data.uuid}>
                <Card.Header>Title: {data.title} <img src={NodeServerURL + data.images.full} style={{ objectFit: "cover", height: "150px", width: "100%" }} /></Card.Header>
                <Card.Body>
                    <p>Description: {data.description}</p>
                    <p>Directions:</p>
                    <span>Marked with * if optional</span>
                    <ul>
                        {data.directions.map(d => {
                            if (d.optional == false) {
                                return (
                                    <li key={d.id}>{d.instructions}*</li>
                                )
                            } else {
                                return (
                                    <li key={d.id}>{d.instructions}</li>
                                )
                            }
                        })}
                    </ul>
                </Card.Body>
            </Card>
        )
    })
    
    return (
        recipeData
    )
}