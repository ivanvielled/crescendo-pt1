import React from 'react'
import {Container} from 'react-bootstrap'

const NotFoundPage = () => {
    return (
        <Container>
            <h3>Not Found Page</h3>
            <p>
                The page you requrested could not be found,
                either contact your webmaster or try again.
                User your browser's back button to navigate
                to the page you have previously come from.
            </p>
        </Container>
    )
}

export default NotFoundPage