import React, { useState, useEffect } from 'react'
import {
    Container,
    Button,
    ButtonGroup,
    InputGroup,
    Card,
    Row,
    Col
} from 'react-bootstrap'

export default function SpecialPage() {
    const [specials, setSpecials] = useState([])
    const NodeServerURL = 'http://localhost:3001/'

    useEffect(() => {
        fetch(`http://localhost:3001/specials`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data !== null) {
                setSpecials(data)
            }
        })
    }, [])

    // console.log(recipes)

    const specialData = specials.map(data => {
        console.log(data)
        return (
            <Card key={data.uuid}>
                <Card.Header>Title: {data.title}</Card.Header>
                <Card.Body>
                    <p>Description: {data.text}</p>
                </Card.Body>
            </Card>
        )
    })
    
    return (
        specialData
    )
}