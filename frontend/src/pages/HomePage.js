import React, { useState, useEffect } from 'react'
import {
    Container,
    Button,
    ButtonGroup,
    InputGroup,
    Card,
    Row,
    Col
} from 'react-bootstrap'
import Footer from '../components/Footer'

export default function HomePage() {
    return (
        <React.Fragment>
            <h5>Hello World!</h5>
        
            <Footer />
        </React.Fragment>
    )
}