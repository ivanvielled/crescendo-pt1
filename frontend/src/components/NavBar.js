import React from 'react'
import {
    Navbar,
    Nav,
    Container
} from 'react-bootstrap'

const NavBar = (props) => {
	let navigation 

    navigation = (
        <React.Fragment>
            <Nav className="mr-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/recipes">Recipes</Nav.Link>
                <Nav.Link href="/specials">Specials</Nav.Link>
            </Nav>
        </React.Fragment>
    )

    return (
    	<Navbar bg="dark" variant="dark" className="mb-3">
            <Container>
                <Navbar.Brand href="/">Crescendo</Navbar.Brand>
                    <Navbar.Collapse>
                        { navigation }
                    </Navbar.Collapse>
            </Container>
    	</Navbar>
    )
}

export default NavBar