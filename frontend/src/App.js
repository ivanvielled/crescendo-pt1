import React, { useState } from 'react'
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom'
import NavBar from './components/NavBar'
import NotFoundPage from './pages/NotFoundPage'
import HomePage from './pages/HomePage'
import RecipePage from './pages/RecipesPage'
import SpecialPage from './pages/SpecialsPage'

export default function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <NavBar />
        <Switch>
          <Route exact path='/' component={ HomePage }/>
          <Route exact path='/recipes' component={ RecipePage }/>
          <Route exact path='/specials' component={ SpecialPage }/>
          <Route component={ NotFoundPage }/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}
